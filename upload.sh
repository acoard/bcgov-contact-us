#!/bin/bash

# Uploads a specific file to the host. See deploy.sh for general use.

## Usage: upload.sh js/assist-support.js
scp $1 forge@104.236.158.9:/home/forge/default/public/cobrowse/$1
