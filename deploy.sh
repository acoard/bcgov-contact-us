#!/bin/bash

# Examples:
# ./deploy dev      # - dry run
# ./deploy dev go   # - real deploy

# Two places to deploy:
#   1. DEV - Adam's personal Forge server
#   2. TEST - MAXIMUS owned server on OpenShift.
# For TEST, the build still must be triggered via OpenShift, but this commits up all changes.

ERRORSTRING="Error. Please make sure you've indicated correct parameters"
if [ $# -eq 0 ]
    then
        echo $ERRORSTRING;
elif [ $1 == "dev" ]
    then
        if [[ -z $2 ]]
            then
                echo "Running dry-run"
                rsync -arv --exclude ".*/" \
                --dry-run \
                --progress \
                --exclude ".*" \
                --exclude "*.sh" \
                --exclude "*.md" \
                . forge@104.236.158.9:/home/forge/default/public/cobrowse/
        elif [ $2 == "go" ]
            then
                echo "Running actual deploy to DEV server"
                rsync -arv --exclude ".*/" \
                --exclude ".*" \
                --progress \
                --exclude "*.sh" \
                --exclude "*.md" \
                . forge@104.236.158.9:/home/forge/default/public/cobrowse/
        else
            echo $ERRORSTRING;
        fi
elif [ $1 == "live" ]
    then
        echo "Moving files into 1.3-prototype in preparation for OpenShift"
        if [[ -z $2 ]]
            then
                # echo "Running dry-run"
                echo "Dry-run not setup. Must run with 'live go'"
        elif [ $2 == "go" ]
            then
                echo "Running actual deploy"

                cd ~/Dev/maximus/MyGovBC-MSP
                git checkout 1.3-prototype
                git fetch
                git rebase

                rsync -arv --exclude ".*/" \
                --exclude ".*" \
                --progress \
                --exclude "*.sh" \
                --exclude "*.md" \
                ~/Dev/maximus/msp-cobrowse/Gov-2.0-Bootstrap-Skeleton/ \
                ~/Dev/maximus/MyGovBC-MSP/src/demo

                git add src/demo
                git commit -m "Generated Commit for MCAP Demo deployment at `date`";

                # read -p "Push 1.3-prototype to origin 1.3-prototype? " -n 1 -r
                # echo    # (optional) move to a new line
                # if [[ $REPLY =~ ^[Yy]$ ]]
                # then
                #     # git push origin 1.3-prototype
                #     echo ""
                # fi

                echo "Done";
        else
            echo $ERRORSTRING;
        fi
fi